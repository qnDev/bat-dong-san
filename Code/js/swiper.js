var swiper = new Swiper('#header .swiper-container', {
    loop:true,
    autoplay :10000,
    spaceBetween: 30
});
var galleryTop = new Swiper('.gallery-top', {
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 10,
    loop:true,
    loopedSlides: 6, //looped slides should be the same     
    centeredSlides:true
});
var galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 10,
    slidesPerView: 4,
    touchRatio: 0.2,
    loop:true,
    loopedSlides: 6, //looped slides should be the same
    slideToClickedSlide: true,
    centeredSlides:true,
});
galleryTop.params.control = galleryThumbs;
galleryThumbs.params.control = galleryTop;


if(window.innerWidth <= 768 && window.innerWidth > 480){
    var swiper = new Swiper('#flat .swiper-container', {
    slidesPerView: 2,
    paginationClickable: true,
    spaceBetween: 30,
    grabCursor: true,
    nextButton: '.swiper-button-next.flat',
    prevButton: '.swiper-button-prev-1.flat',
    loop: true
});


var swiper = new Swiper('#house .swiper-container', {
    slidesPerView: 2,
    paginationClickable: true,
    spaceBetween: 30,
    grabCursor: true,
    nextButton: '.swiper-button-next.house',
    prevButton: '.swiper-button-prev-1.house',
    loop: true
});

var swiper = new Swiper('#land .swiper-container', {
    slidesPerView: 2,
    paginationClickable: true,
    spaceBetween: 30,
    grabCursor: true,
    nextButton: '.swiper-button-next.land',
    prevButton: '.swiper-button-prev-1.land',
    loop: true
});




var swiper = new Swiper('#partner .swiper-container', {
    slidesPerView: 5,
    loop:true,
    autoplay :1000,
    
});
}else if(window.innerWidth <= 480){
var swiper = new Swiper('#flat .swiper-container', {
    slidesPerView: 1,
    paginationClickable: true,
    spaceBetween: 30,
    grabCursor: true,
    nextButton: '.swiper-button-next.flat',
    prevButton: '.swiper-button-prev-1.flat',
    loop: true
});


var swiper = new Swiper('#house .swiper-container', {
    slidesPerView: 1,
    paginationClickable: true,
    spaceBetween: 30,
    grabCursor: true,
    nextButton: '.swiper-button-next.house',
    prevButton: '.swiper-button-prev-1.house',
    loop: true
});

var swiper = new Swiper('#land .swiper-container', {
    slidesPerView: 1,
    paginationClickable: true,
    spaceBetween: 30,
    grabCursor: true,
    nextButton: '.swiper-button-next.land',
    prevButton: '.swiper-button-prev-1.land',
    loop: true
});


var swiper = new Swiper('#partner .swiper-container', {
    slidesPerView: 3,
    loop:true,
    autoplay :1000,
    
});
}
 



else{
    var swiper = new Swiper('#flat .swiper-container', {
    slidesPerView: 4,
    paginationClickable: true,
    spaceBetween: 30,
    grabCursor: true,
    nextButton: '.swiper-button-next.flat',
    prevButton: '.swiper-button-prev-1.flat',
    loop: true
});


var swiper = new Swiper('#house .swiper-container', {
    slidesPerView: 4,
    paginationClickable: true,
    spaceBetween: 30,
    grabCursor: true,
    nextButton: '.swiper-button-next.house',
    prevButton: '.swiper-button-prev-1.house',
    loop: true
});

var swiper = new Swiper('#land .swiper-container', {
    slidesPerView: 4,
    paginationClickable: true,
    spaceBetween: 30,
    grabCursor: true,
    nextButton: '.swiper-button-next.land',
    prevButton: '.swiper-button-prev-1.land',
    loop: true
});


var swiper = new Swiper('#partner .swiper-container', {
    slidesPerView: 7,
    loop:true,
    autoplay :1000,
    
});
}

var swiper = new Swiper('#reviews .swiper-container', {
    slidesPerView: '1',
    centeredSlides: true,
    spaceBetween: 30,
    autoplay:10000,
});